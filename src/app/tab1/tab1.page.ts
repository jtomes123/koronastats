import { Component } from '@angular/core'
import { DataService } from '../data-service.service'
import { ChartDataSets } from 'chart.js'
import { Label, Color } from 'ng2-charts'
import { LanguageService, Localization } from '../language.service'
import { Platform } from '@ionic/angular'
import { CookieService } from 'ngx-cookie-service'

@Component({
    selector: 'app-tab1',
    templateUrl: 'tab1.page.html',
    styleUrls: ['tab1.page.scss'],
})
export class Tab1Page {
    Localization = Localization
    infectedChart: ChartDataSets[] = [
        {
            data: [],
            label: this.languageService.localize(Localization.Infected),
        },
    ]
    testedChart: ChartDataSets[] = [
        { data: [], label: this.languageService.localize(Localization.Tested) },
    ]
    infectedPerDayChart: ChartDataSets[] = [
        {
            data: [],
            label: this.languageService.localize(Localization.InfectedPerDay),
        },
    ]
    infectedByAgeMenChart: ChartDataSets[] = [
        {
            data: [],
            label: this.languageService.localize(Localization.InfectedPerDay),
        },
    ]
    infectedByAgeWomenChart: ChartDataSets[] = [
        {
            data: [],
            label: this.languageService.localize(Localization.InfectedPerDay),
        },
    ]
    infectedTestedPieChart: ChartDataSets[] = [{ data: [] }]
    chartOptions = {
        responsive: true,
        tooltips: {
            enabled: false,
        },
        scales: {
            yAxes: [
                {
                    ticks: {
                        autoSkip: true,
                        autoSkipPadding: 50,
                    },
                    display: this.showPieLegends ? 'auto' : false,
                    type: 'linear',
                    beforeBuildTicks: (scl) => {
                        const max = Math.max.apply(
                            Math,
                            scl.chart.config.data.datasets[0].data
                        )
                        scl.options.ticks.max = Math.ceil(max * 1.35)
                        scl.options.display = this.showPieLegends
                            ? 'auto'
                            : false
                    },
                },
            ],
        },
        plugins: {
            datalabels: {
                font: { size: 16 },
                anchor: 'start',
                align: 225,
                offset: 4,
                display: (context) => {
                    const max = Math.max.apply(Math, context.dataset.data)
                    const val = context.dataset.data[context.dataIndex]
                    const length = context.dataset.data.length

                    const offset = length % 2 ? 1 : 0

                    console.log(context.dataset.data)
                    console.log(max)
                    console.log(val)

                    if (context.dataIndex == length - 1) {
                        return true
                    }

                    return context.dataIndex != 0 ? 'auto' : false
                },
                clamp: true,
            },
        },
    }
    infectedPerDayLabels: Label[] = []
    barOptions = {
        responsive: true,
        tooltips: {
            enabled: false,
        },
        scales: {
            yAxes: [
                {
                    ticks: {
                        autoSkip: true,
                        autoSkipPadding: 50,
                    },
                    beforeFit: (scl) => {
                        const max = Math.max.apply(
                            Math,
                            scl.chart.config.data.datasets[0].data
                        )
                        scl.options.display = this.showPieLegends
                        scl.options.ticks.max = Math.ceil(max * 1.35)
                    },
                },
            ],
        },
        plugins: {
            datalabels: {
                font: { size: 16 },
                anchor: 'end',
                align: 'bottom',
                display: 'auto',
                clip: 'false',
                offset: -32,
            },
        },
    }
    pieOptions = {
        responsive: true,
        circumference: Math.PI,
        rotation: -Math.PI,
        tooltips: {
            enabled: false,
        },
        plugins: {
            datalabels: {
                formatter: (value, ctx) => {
                    const datasets = ctx.chart.data.datasets
                    if (datasets.indexOf(ctx.dataset) === datasets.length - 1) {
                        const sum = datasets[0].data.reduce((a, b) => a + b, 0)
                        const percentage = Math.round((value / sum) * 100) + '%'
                        return percentage
                    } else {
                        return 'N/A'
                    }
                },
                font: { size: this.showPieLegends ? 20 : 10 },
                display: true,
            },
        },
    }
    infectedChartColors: Color[] = [
        {
            borderColor: '#ff6347',
            backgroundColor: '#ff634755',
        },
    ]
    infectedPerDayChartColors: Color[] = [
        {
            borderColor: '#ffff75',
            backgroundColor: '#ffff75',
        },
    ]
    testedChartColors: Color[] = [
        {
            borderColor: '#40e0d0',
            backgroundColor: '#40e0d055',
        },
    ]
    pieChartColors: Color[] = [
        {
            borderColor: [
                '#40e0d0',
                '#A5E6BA',
                '#9AC6C5',
                '#F7E3AF',
                '#F7AF9D',
                '#C08497',
                '#BF5979',
                '#AF4969',
                '#9F3959',
            ],
            backgroundColor: [
                '#40e0d0',
                '#A5E6BA',
                '#9AC6C5',
                '#F7E3AF',
                '#F7AF9D',
                '#C08497',
                '#BF5979',
                '#AF4969',
                '#9F3959',
            ],
        },
    ]
    infectedTestedChartColors: Color[] = [
        {
            borderColor: ['#ff6347', '#40e0d0'],
            backgroundColor: ['#ff6347', '#40e0d0'],
        },
    ]
    infectedChartLabels: Label[] = []
    infectedByAgeMenLabels: Label[] = []
    infectedByAgeWomenLabels: Label[] = []
    infectedTestedLabels: Label[] = [
        this.languageService.localize(Localization.Infected),
        this.languageService.localize(Localization.Tested),
    ]
    chartType = 'line'
    showLegend = false
    infected = 0
    recovered = 0
    tested = 0
    deceased = 0
    isLoading = true
    date = 'N/A'
    showDates = true

    get showPieLegends(): boolean {
        return this.platform.width() > 512
    }

    constructor(
        private dataService: DataService,
        public languageService: LanguageService,
        private platform: Platform,
        private cookies: CookieService
    ) {
        if (cookies.check('dates')) {
            switch (cookies.get('dates')) {
                case 't':
                    this.showDates = true
                    break
                default:
                    this.showDates = false
                    break
            }
        } else {
            cookies.set('dates', 'f')
            this.showDates = false
        }
        this.updateData()
    }

    showDatesChanged() {
        this.showDates
            ? this.cookies.set('dates', 't')
            : this.cookies.set('dates', 'f')

        this.updateData()
    }
    updateData() {
        this.dataService.getData().subscribe((data) => {
            this.isLoading = true
            console.log(data)
            const infected = []
            const tested = []
            const infectedPerDay = []
            const labels = []

            const sliceIndex =
                data.totalPositiveTests.indexOf(
                    data.totalPositiveTests.find((e) => e.value > 0)
                ) - 1
            const validData = data.totalPositiveTests.slice(sliceIndex)
            const validTestedData = data.numberOfTestedGraph.slice(sliceIndex)

            for (const element of validData) {
                const index = validData.indexOf(element)
                infected.push((element as any).value)
                if (index < validTestedData.length) {
                    tested.push((validTestedData[index] as any).value)
                }
                if (index < 1) {
                    infectedPerDay.push((element as any).value)
                } else {
                    infectedPerDay.push(
                        (element as any).value - validData[index - 1].value
                    )
                }
                if (this.showDates) {
                    const date = new Date(element.date)
                    labels.push(date.toLocaleDateString())
                } else {
                    labels.push(
                        this.languageService.localize(Localization.Day) +
                            ' ' +
                            index
                    )
                }
            }
            const menValues: number[] = []
            const womenValues: number[] = []
            this.infectedByAgeMenLabels = []
            this.infectedByAgeWomenLabels = []
            data.infectedByAgeSex.forEach((sexData) => {
                if (sexData.sex === 'muž') {
                    sexData.infectedByAge.forEach((menAgeGroup, index, arr) => {
                        this.infectedByAgeMenLabels.push(menAgeGroup.age)
                        menValues.push(menAgeGroup.value)
                    })
                } else if (sexData.sex === 'žena') {
                    sexData.infectedByAge.forEach(
                        (womenAgeGroup, index, arr) => {
                            this.infectedByAgeWomenLabels.push(
                                womenAgeGroup.age
                            )
                            womenValues.push(womenAgeGroup.value)
                        }
                    )
                }
            })
            console.log(tested)

            this.infectedChart = [
                {
                    data: infected,
                    label: this.languageService.localize(Localization.Infected),
                    pointHitRadius: 5,
                    pointRadius: 0,
                },
            ]
            this.testedChart = [
                {
                    data: tested,
                    label: this.languageService.localize(Localization.Tested),
                    pointRadius: 0,
                    pointHitRadius: 5,
                },
            ]
            this.infectedPerDayChart = [
                {
                    data: infectedPerDay.slice(infectedPerDay.length - 10),
                    label: this.languageService.localize(Localization.Infected),
                    pointRadius: 0,
                    pointHitRadius: 5,
                },
            ]
            this.infectedPerDayLabels = labels.slice(labels.length - 10)
            this.infectedByAgeMenChart = [
                { data: menValues, hoverBorderWidth: 15 },
            ]
            this.infectedByAgeWomenChart = [
                { data: womenValues, hoverBorderWidth: 15 },
            ]

            this.infectedChartLabels = labels
            this.tested =
                data.numberOfTestedGraph[
                    data.numberOfTestedGraph.length - 1
                ].value
            this.recovered = data.recovered
            this.infected =
                data.totalPositiveTests[
                    data.totalPositiveTests.length - 1
                ].value
            this.deceased = data.deceased
            this.date = new Date(data.lastUpdatedAtApify).toLocaleString(
                this.languageService.CurrentLanguage
            )
            this.infectedTestedPieChart = [
                {
                    data: [this.infected, this.tested - this.infected],
                    hoverBorderWidth: 15,
                },
            ]
            this.isLoading = false
        })
    }
}
