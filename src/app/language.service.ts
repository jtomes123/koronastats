import { Injectable } from '@angular/core'
import { CookieService } from 'ngx-cookie-service'

export enum Language {
    English = 'en',
    Czech = 'cz',
}

export enum Localization {
    Day = 'day',
    Infected = 'infected',
    InfectedPerDay = 'infectedPerDay',
    Cured = 'cured',
    Charts = 'charts',
    Regions = 'regions',
    Settings = 'config',
    Statistics = 'stats',
    Tested = 'tested',
    Language = 'language',
    Quarantened = 'quarantened',
    WomenByAge = 'wbyage',
    MenByAge = 'mbyage',
    InfectedToTested = 'inftotest',
    LastUpdate = 'lstup',
    MadeBy = 'madeby',
    Deceased = 'deceased',
    DataSource = 'datasrc',
    ShowDates = 'showdates',
    Name = 'name',
    Email = 'email',
    Feedback = 'feedback',
    Send = 'send',
    FeedbackProcessing = 'feedbackProcessing',
    FeedbackSuccess = 'feedbackSuccess',
    FeedbackFail = 'feedbackFail',
}

const czech = {
    day: 'Den',
    infected: 'Pozitivně testovaní',
    infectedPerDay: 'Pozitivně testovaní za den',
    charts: 'Grafy',
    config: 'Nastavení',
    regions: 'Regiony',
    stats: 'Statistiky',
    tested: 'Testovaní',
    cured: 'Vyléčení',
    language: 'Jazyk',
    quarantened: 'V karanténě',
    mbyage: 'Muži dle věku',
    wbyage: 'Ženy dle věku',
    inftotest: 'Poměr pozitivních a negativních testů',
    lstup: 'Naposledy aktualizováno',
    madeby: 'Vytvořil',
    deceased: 'Mrtví',
    datasrc: 'Zdroj dat',
    showdates: 'Zobrazit data (nebo dny od 1. března)',
    name: 'Jméno',
    email: 'Email',
    feedback: 'Zpětná vazba',
    send: 'Odeslat',
    feedbackProcessing: 'Odesílám...',
    feedbackSuccess: 'Děkujeme za vaší zpětnou vazbu.',
    feedbackFail: 'Vyskytla se chyba, prosím zkuste to později.',
}

const english = {
    day: 'Day',
    infected: 'Positively tested',
    infectedPerDay: 'Positively tested per day',
    charts: 'Charts',
    config: 'Settings',
    regions: 'Regions',
    stats: 'Statistics',
    tested: 'Tested',
    cured: 'Recovered',
    language: 'Language',
    quarantened: 'Quarantined',
    mbyage: 'Men by age',
    wbyage: 'Women by age',
    inftotest: 'Positively tested to tested',
    lstup: 'Last updated',
    madeby: 'Made by',
    deceased: 'Deceased',
    datasrc: 'Data source',
    showdates: 'Show dates (or days from 1st of March)',
    name: 'Name',
    email: 'Email',
    feedback: 'Feedback',
    send: 'Send',
    feedbackSuccess: 'Thank you for your feedback.',
    feedbackFail: 'There was an error, please try again later.',
    feedbackProcessing: 'Sending...',
}

@Injectable({
    providedIn: 'root',
})
export class LanguageService {
    private langPack: any
    private currentLang: Language

    constructor(private cookies: CookieService) {
        if (cookies.check('lang')) {
            this.CurrentLanguage = cookies.get('lang') as Language
        } else {
            this.CurrentLanguage = Language.Czech
        }
    }

    localize(tag: Localization): string {
        if (tag in this.langPack) {
            return this.langPack[tag]
        } else if (tag in english) {
            console.warn(
                'Tag ',
                tag,
                ' does not exits in selected language pack!'
            )
            return english[tag]
        } else {
            console.error('Tag ', tag, ' does not exits!')
            return tag
        }
    }

    get CurrentLanguage(): Language {
        return this.currentLang
    }

    set CurrentLanguage(value: Language) {
        switch (value) {
            case Language.Czech:
                this.langPack = czech
                this.currentLang = value
                break

            default:
                this.langPack = english
                this.currentLang = Language.English
                break
        }
        if (this.cookies.check('lang')) {
            this.cookies.delete('lang')
        }
        this.cookies.set('lang', this.currentLang)
    }
}
