import { Component } from "@angular/core";
import { DataService } from "../data-service.service";
import { LanguageService, Localization, Language } from "../language.service";

class Region {
  name: string;
  quarantined: number;
  infected: number;
}

@Component({
  selector: "app-tab2",
  templateUrl: "tab2.page.html",
  styleUrls: ["tab2.page.scss"]
})
export class Tab2Page {
  Localization = Localization;
  Language = Language;
  regions: Region[];
  isLoading = true;
  date = "N/A";
  constructor(private dataService: DataService, public lang: LanguageService) {
    this.updateData();
  }

  updateData() {
    this.dataService.getData().subscribe(data => {
      this.isLoading = true;
      const regions: Region[] = [];
      data.infectedByRegion.forEach(element => {
        regions.push({
          name: element.name,
          infected: element.value,
          quarantined: -1
        });
      });
      if (data.regionQuarantine.length > 0) {
        data.regionQuarantine[
          data.regionQuarantine.length - 1
        ].regionData.forEach(element => {
          const myRegion = regions.find(r => r.name === element.regionName);
          if (!myRegion) {
            regions.push({
              name: element.regionName,
              infected: -1,
              quarantined: element.value
            });
          } else {
            myRegion.quarantined = element.value;
          }
        });
      }
      const tempDate = new Date(data.lastUpdatedAtApify);

      this.date = new Date(data.lastUpdatedAtApify).toLocaleString(
        this.lang.CurrentLanguage
      );
      this.regions = regions;
      this.isLoading = false;
    });
  }
}
