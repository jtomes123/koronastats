import { Component } from '@angular/core'
import { Localization, LanguageService, Language } from '../language.service'

@Component({
    selector: 'app-tab3',
    templateUrl: 'tab3.page.html',
    styleUrls: ['tab3.page.scss'],
})
export class Tab3Page {
    Localization = Localization
    Language = Language
    language: Language

    question1: number = 1
    question2: number = 2
    answer: number = 0
    wrong: boolean = false

    author: string = ''
    email: string = ''
    feedback: string = ''

    authorWrong: boolean = false
    emailWrong: boolean = false
    feedbackWrong: boolean = false

    feedbackMessage: string = ''

    sendClicked: boolean = false

    constructor(public lang: LanguageService) {
        this.language = lang.CurrentLanguage
        this.question1 = Math.round(Math.random() * 10)
        this.question2 = Math.round(Math.random() * 10)
    }
    updateLang(selectedLanguage: any) {
        console.log(selectedLanguage.detail.value)
        this.lang.CurrentLanguage = selectedLanguage.detail.value
        console.log(this.lang.CurrentLanguage)
    }
    send() {
        let hadErrors = false

        if (!this.author || this.author === '') {
            this.authorWrong = true
            hadErrors = true
        } else {
            this.authorWrong = false
        }
        if (!this.feedback || this.feedback === '') {
            this.feedbackWrong = true
            hadErrors = true
        } else {
            this.feedbackWrong = false
        }

        if (
            this.email &&
            !this.email.includes('@') &&
            !this.email.includes('.')
        ) {
            this.emailWrong = true
            hadErrors = true
        } else {
            this.emailWrong = false
        }

        if (!(this.question1 + this.question2 === this.answer)) {
            this.wrong = true
            hadErrors = true
        } else {
            this.wrong = false
        }

        if (hadErrors) return

        var myHeaders = new Headers()
        myHeaders.append('Content-Type', 'application/json')

        var raw = JSON.stringify({
            message: this.feedback,
            author: this.author,
            email: this.email === '' ? 'null' : this.email,
        })

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
        }

        this.feedbackMessage = this.lang.localize(
            Localization.FeedbackProcessing
        )
        this.sendClicked = true

        fetch('https://korona.tomes.dev/addfeedback', requestOptions)
            .then((response) => response.text())
            .then((result) => {
                console.log(result)
                this.feedbackMessage = this.lang.localize(
                    Localization.FeedbackSuccess
                )
            })
            .catch((error) => {
                console.log('error', error)
                this.feedbackMessage = this.lang.localize(
                    Localization.FeedbackFail
                )
            })
    }
}
