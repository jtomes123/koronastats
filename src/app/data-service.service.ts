import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable, Timestamp } from "rxjs";

export interface CountryData {
  countryName: string;
  value: number;
}
export interface RegionData {
  name: string;
  value: number;
}
export interface DatedData {
  value: number;
  date: string;
}
export interface AgeData {
  age: string;
  value: number;
}
export interface RegionQuarantineReportData {
  regionName: string;
  value: number;
}
export interface RegionQuarantineReport {
  reportDate: string;
  regionData: RegionQuarantineReportData[];
}
export interface InfectedByAgeSex {
  sex: string;
  infectedByAge: AgeData[];
}

export interface IApiData {
  countryOfInfection: CountryData[];
  fromBabisNewspapers: Object;
  infected: number;
  infectedByAgeSex: InfectedByAgeSex[];
  infectedByRegion: RegionData[];
  infectedDaily: DatedData[];
  lastUpdatedAtApify: string;
  lastUpdatedAtSource: string;
  numberOfTestedGraph: DatedData[];
  readMe: string;
  recovered: number;
  regionQuarantine: RegionQuarantineReport[];
  totalPositiveTests: DatedData[];
  totalTested: number;
  deceased: number;
}

@Injectable({
  providedIn: "root"
})
export class DataService {
  constructor(private http: HttpClient) {}

  getData(): Observable<IApiData> {
    return this.http.get<IApiData>(
      "https://api.apify.com/v2/key-value-stores/K373S4uCFR9W1K8ei/records/LATEST?disableRedirect=true"
    );
  }
}
