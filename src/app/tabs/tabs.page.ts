import { Component } from "@angular/core";
import { LanguageService, Localization } from "../language.service";
import { AppComponent } from "../app.component";
import { Platform } from "@ionic/angular";

@Component({
  selector: "app-tabs",
  templateUrl: "tabs.page.html",
  styleUrls: ["tabs.page.scss"]
})
export class TabsPage {
  Localization = Localization;
  tabsPlacement = "bottom";
  tabsLayout = "standard";
  constructor(public lang: LanguageService, private platform: Platform) {
    if (!this.platform.is("mobile")) {
      this.tabsPlacement = "top";
      this.tabsLayout = "icon-left";
    }
  }
}
